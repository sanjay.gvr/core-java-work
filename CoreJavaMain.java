package main.corejava;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CoreJavaMain {

	public static void main(String[] args) {
		
		System.out.println("Working on symmetric difference algorithm using collections");

		Integer first[] = {1,2,5};
		
		Integer second[] = {1,2,3};
		
		Set<Integer> finalSetter = getSymDiff(first, second);
		
		System.out.println("finalSetter is : " + finalSetter);
		
	}
	
	private static <T> Set<T> getSymDiff(T first[], T second[]) {
		
		Set<T> diff = new HashSet<>();
//		List<Integer> lister = Arrays.asList(first);
//		Set<Integer> firstSet1 = Arrays.stream(first).collect((Supplier<R>) Collectors.toSet());
		
		
		Set<T> firstSet = new HashSet<>(Arrays.asList(first));
		Set<T> secondSet = new HashSet<>(Arrays.asList(second));
		
		
		Map<T, Integer> finalMap = new HashMap<T,Integer>();
		
		firstSet.forEach(e -> putKey(finalMap, e));
		secondSet.forEach(e -> putKey(finalMap, e));
		
//		Map<T, Integer> filerMap = new HashMap<T,Integer>(); 
		
//		filerMap = finalMap.entrySet().stream().filter(e -> e.getValue() == 1 ).collect(Collectors.toMap(T, Integer));
		
		diff = finalMap.entrySet().stream().filter(e -> e.getValue() == 1 ).map(Map.Entry::getKey).collect(Collectors.toSet());
		
		return diff;
		
	}
	
	private static <T> void putKey(Map<T, Integer> finalMap, T key){
		
		if (finalMap.containsKey(key)) {
			finalMap.replace(key, Integer.MAX_VALUE);
		}
		else {
			finalMap.put(key, 1);
		}
		
	}
}
